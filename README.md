Repositório de Código para AMSI 2016/17
=======

Repositório com exemplos, exercícios e sugestões de implementação usadas nas 
aulas da Unidade Curricular Acesso Móvel a Sistemas de Informação, do Curso Técnico 
Superior Profissional de Programação de Sistemas de Informação da Escola Superior 
de Tecnologia e Gestão, IPL.

## Nota

O código presente neste repositório foi desenvolvido especificamente para a UC e não 
deve ser usado como base para qualquer estudo ou aprendizagem não supervisionada. Os 
projetos são implementados de acordo com os objetivos e características das aulas, do 
seu planeamento e da evolução em sala dos alunos. Contém erros intencionais e implementações 
simplistas ou não aplicáveis fora do âmbito da UC.

## Licensa

Disponibilizado ao abrigo da licença Creative Commons (CC) Atribuição-NãoComercial 4.0 
Internacional. Consultar https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.
